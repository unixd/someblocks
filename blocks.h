static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"[  ", "date +'%d:%m'",							1,		0},
	{"]", " ",											0,		0},
	{"[  ", "date '+%H:%M'",							1,		0},
	{"]", " ",											0,		0},
};

static char delim[] = " | ";
static unsigned int delimLen = 1;